#include <iostream>
#include <stack>
#include <string>
#include <vector>

using namespace std;

bool checker(string args)
{
    std::stack<char> temporaryChecker;
    char temporaryChar;
    for (int i = 0; i < args.length(); i++)
    {
        if (args[i] == '(' || args[i] == '[' || args[i] == '{')
        {
            temporaryChecker.push(args[i]);
            continue;
        }
        switch (args[i]) {
        case ')':
            temporaryChar = temporaryChecker.top();
            temporaryChecker.pop();
            if (temporaryChar != '(')
                return false;
            break;

        case '}':
            temporaryChar = temporaryChecker.top();
            temporaryChecker.pop();
            if (temporaryChar != '{')
                return false;
            break;
        case ']':
            temporaryChar = temporaryChecker.top();
            temporaryChecker.pop();
            if (temporaryChar != '[')
                return false;
            break;
        }
    }
    return (temporaryChecker.empty());
}
int main(int argc, char *argv[])
{
    std::string arguments;
    for(int i=1; i<argc; i++){
        arguments += argv[i];
    }

    if (checker(arguments))
        std::cout << "OK";
    else
        std::cout << "ERROR";
    return 0;
}
