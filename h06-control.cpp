#include <iostream>
#include <stdio.h>
using namespace std;

class Game{
    private:
        int x_position;
        int y_position;
        int map_width;
        int map_height;
        void change_x(int change){
            if (x_position + change < 1 || x_position + change > map_width) {}
            else {
                x_position += change;
            }
        }
        void change_y(int change){
            if (y_position + change < 1 || y_position + change > map_height) {}
            else {
                y_position += change;
            }
        }
        int get_x() const{
            return x_position;
        }
        int get_y() const{
            return y_position;
        }
        int get_map_width() const{
            return map_width;
        }
        int get_map_height() const{
            return map_height;
        }

    public:
        Game(int x, int y, int w, int h){
            x_position = x;
            y_position = y;
            map_width = w;
            map_height = h;
        }
        int move(char input) {
            switch (input) {
                case 'w':
                    change_y(1);
                    break;
                case 's':
                    change_y(-1);
                    break;
                case 'a':
                    change_x(-1);
                    break;
                case 'd':
                    change_x(1);
                    break;
                case 'q':
                    return 0;
                    break;
                default:
                    cout<<"Wrong key";
            }
            return 1;
        }
        void printer() {
            for(int i = get_map_height()+1; i >= 0; i--){
                if(i == 0 || i == get_map_height()+1){
                    for(int z=0; z<=get_map_width()+1; z++)
                        cout<<"#";      //Obrysowanie planszy poziomo
                }
                else{
                    for(int k = 0; k <= get_map_width()+1; k++){
                        if(k == 0 || k == get_map_width()+1){
                            cout<<"#";      // Obrysowanie planszy pionowo
                        }
                        else if(get_x()==k && get_y()==i)
                            cout<<"*";

                        else
                            cout<<" ";
                    }
                }
                cout<<"\n";
            }
        }
};

int main(int argc, char **argv) {
    int default_width = 10;
    int default_height = 10;

    if(argc > 1){
        default_width = atoi(argv[1]);
        default_height = atoi(argv[2]);
    } //Zakładając, że argumenty to tylko wymiary mapy

    Game snake(1,1, default_width, default_height);
    // Mapa jest rozszeżona o 1 w każdą stronę na potrzeby obrysowania, pozycja startowa to 0/0 pomimo zmiennej 1/1

    int game_working=1;
    snake.printer();
    while(game_working!=0){
        system("stty raw");
        game_working = snake.move(getchar());
        system("stty cooked");
        system("clear");
        snake.printer();
    };
    return 0;
}
