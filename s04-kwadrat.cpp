#include <iostream>
#include <iomanip>

using namespace std;

struct Rectangle{      
    float width, height;  
    
    Rectangle(float x, float y)      
    {      
        width = x;      
        height = y;      
    }  
 
    auto draw() const -> void{
        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++)
                cout << "*";
        cout << endl;
        }
    }
 
    auto scale(float const x, float const y) -> void{
        width = width*x;
        height = height*y;
    }

    auto area() const -> float {
        return width*height;
    }
 };   

int main(int argc, char *argv[]) {
    
    float x=atof(argv[1]);
    float y=atof(argv[2]);
    float scale_x=atof(argv[3]);
    float scale_y=atof(argv[4]);
    
    struct Rectangle example=Rectangle(x,y); 
    example.scale(scale_x, scale_y);
    example.draw();
    
    return 0;

}
