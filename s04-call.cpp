#include <iostream>
#include <stdlib.h>

auto get_random_int() -> int
{
    return rand() % 100;
}

auto some_function(int const random) -> void
{
    std::cout << "some_function : " << random << std::endl;
}

auto call_with_random_int(void (*fp)(int const)) -> void
{
    (*fp)(get_random_int()); 
}

auto main() -> int
{
    auto fp = &some_function;
    call_with_random_int(fp);
    
    return 0;
}
